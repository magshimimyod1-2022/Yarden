import socket
from datetime import date


def get_sum(num):
    # function that get a number and sum his digit example: "123", 1+2+3 = 6
    sum_num = 0
    for digit in str(num):
        sum_num += int(digit)
    return sum_num


def gematria(word):
    # function that calculate sum of given word by letters if a = 1, b = 2, c =3...
    alphabet = {"a": 1, "b": 2, "c": 3, "d": 4, "e": 5, "f": 6, "g": 7, "h": 8, "i": 9, "j": 10, "k": 11, "l": 12,
                "m": 13,
                "n": 14, "o": 15, "p": 16, "q": 17, "r": 18, "s": 19, "t": 20, "u": 21, "v": 22, "w": 23, "x": 24,
                "y": 25, "z": 26, " ": 0}
    word_gematria = 0
    for i in range(len(word)):
        word_gematria += alphabet[word[i]]
    return word_gematria


def calculating_weather(city, date):
    # function that get to server and send requests for weather situation
    SERVER_IP = "34.218.16.79"
    SERVER_PORT = 77
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)

    checksum_first_half = ''.join(i for i in date if i.isdigit())
    checksum_first_half = get_sum(checksum_first_half)
    checksum_second_half = gematria(city)
    checksum = str(checksum_second_half) + "." + str(checksum_first_half)

    msg = "100:REQUEST:city=" + city + "&date=" + date + "&checksum=" + checksum
    sock.sendall(msg.encode())
    # first message is "hello" type message
    welcome_msg = sock.recv(1024)
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()
    # split the response to section and using the & format
    server_msg = server_msg.split("&")
    for word in server_msg:
        if "temp" in word:
            temp = word[5:]
        if "text" in word:
            weather = word[5:]
    new_tuple = weather, temp
    print(new_tuple)


def main():
    today = date.today()
    i = 4
    j = 1
    city_user = input("Please insert city: ")
    choice = int(input("first option: see today's weather.\n"
                       "second option: see the forecast for the next 3 days.\n"
                       "choose 1 or 2\n"
                       "Your answer: "))
    # get current date by the right format. BTW this is a cool function!
    date_user = today.strftime("%d/%m/%Y")
    if choice == 1:
        calculating_weather(city_user.lower(), date_user)
    elif choice == 2:
        calculating_weather(city_user.lower(), date_user)
        while i != 1:
            day = str(int(date_user[0:2]) + j)
            final_date = date_user.replace(date_user[0:2], day)
            calculating_weather(city_user.lower(), final_date)
            i -= 1
            j += 1


if __name__ == "__main__":
    main()
