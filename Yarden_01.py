import requests
from bs4 import BeautifulSoup
from collections import Counter


def extract_password_from_site():
    """
    function that extract the 100th letter
    from each file on a site
    :return: string of password
    """
    password_string = ""
    url = "http://webisfun.cyber.org.il/nahman/files"
    reqs = requests.get(url)
    soup = BeautifulSoup(reqs.text, 'html.parser')
    for link in soup.find_all('a'):
        # combine url with links on the site so i can use each one of them to requests
        url = "http://webisfun.cyber.org.il/nahman/files" + "/" + link.get('href')
        if "nfo" in url:
            r = requests.post(url)
            password_string += str(r.text[99])
    return password_string


def find_most_common_words(data_set, k):
    """
    function that find most common words by order and adding them to string
    :param data_set: path
    :param k: amount of most words
    :return: string of most commons
    """
    final_string = ""
    with open(data_set, 'r') as data:
        data = data.readline()
        split_data = data.split()
        counter = Counter(split_data)
        most_occur = counter.most_common(k)
        for item in most_occur:
            for word in item:
                final_string += str(word) + " "
                break
        return final_string[:-1]


def main():
    choice = input("""what func would you like to execute?
    choose 'one' for extract_password_from_site
    choose 'two' for find_most_common_words
    ... """)
    if choice == '1':
        print(extract_password_from_site())
    elif choice == '2':
        data_set = "words.txt"
        amount = 6
        print(find_most_common_words(data_set, amount))


if __name__ == "__main__":
    main()
