import socket

LISTEN_PORT = 9090
SERVER_IP = "54.71.128.194"
SERVER_PORT = 92

while True:
    # Create a listening socket
    listening_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('', LISTEN_PORT)
    listening_sock.bind(server_address)
    listening_sock.listen(1)

    # Create a new conversation socket
    client_soc, client_address = listening_sock.accept()

    # Create TCP socket with server
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER_IP, SERVER_PORT)
    server_sock.connect(server_address)

    # getting info from client
    client_msg = client_soc.recv(1024)
    client_msg = client_msg.decode()
    print(client_msg)

    server_sock.sendall(client_msg.encode())

    # getting response from server
    server_msg = server_sock.recv(1024)
    server_msg = server_msg.decode()

    server_msg = server_msg.replace("jpg", ".jpg")

    if "ERROR" in server_msg:
        server_msg = server_msg.split("#")
        server_msg[0] = "ERROR"
        server_msg = '#'.join(server_msg)

    if "France" in server_msg:
        server_msg = 'ERROR#"France is banned!"'

    # bonus
    if "ERROR" not in server_msg:
        rating = server_msg.split("&")
        rating = rating[4]
        rating = rating[-3:-1] + rating[-1]

        name = server_msg.split("&")
        name = name[0]
        name = name.split("#")
        name = name[1]
        if float(rating) < 6.5:
            server_msg = 'ERROR#"The movie %s is bad, rating is lower then 6.5 star, only %s!"' % (name, rating)

    print(server_msg)

    server_sock.close()
    client_soc.send(server_msg.encode())
    client_soc.close()
