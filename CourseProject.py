def is_valid_input(letter_guessed):
    """checking if letter is valid oor not
     parm letter_guess: user guess
     type letter_guess: string
     return: True of False
     rtype: bool"""

    if letter_guessed.isalpha():
        if len(letter_guessed) == 1:
            return True
        else:
            return False

    else:
        return False


def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """checking if letter is valid or not and already guessed
     parm letter_guess: user guess
     parm old_letters_guessed: list of guessed letters
     type letter_guess: string
     type old_letters_guessed: list
     return: True of False
     rtype: bool"""

    if letter_guessed.isalpha():
        if letter_guessed.lower() in old_letters_guessed:
            false(old_letters_guessed, letter_guessed)
            return False
        elif len(letter_guessed) == 1:
            return True
        else:
            false(old_letters_guessed, letter_guessed)
            return False
    else:
        false(old_letters_guessed, letter_guessed)
        return False


def false(my_list, new_letter):
    """adding wrong guesses and printing
     parm my_list: list of wrong guesses
     parm new_letter: letter that is wrong
     type my_list: list
     type new_letter: string
     return: None
     rtype: None"""
    for i in range(len(my_list)):
        my_list[i] = my_list[i].lower()
    print("X\n" + " -> ".join(sorted(my_list)))
    my_list += new_letter
    return None


def show_hidden_word(secret_word, old_letters_guessed):
    """
    function that replace the correct words with
    their value instead of "_"
    :param secret_word: the word that is being guessed
    :type: string
    :param old_letters_guessed: bank of guessed letters
    :type: list
    :return: final string with "_ " and letters
    :rtype: string
    """
    my_str = ""
    j = 0
    for char in old_letters_guessed:
        j += 1
    for item in secret_word:
        i = 0
        for x in old_letters_guessed:
            if item == x:
                my_str += item + " "
                break
            else:
                i += 1
        if i == j:
            my_str += "_ "
    return my_str[:-1]


def display_hangman(tries):
    stages = [
        """
                    x-------x
                    |       |
                    |       0
                    |      /|\\
                    |      / \\
                    |
                """,
        """
                    x-------x
                    |       |
                    |       0
                    |      /|\\
                    |      /
                    |
                """,
        """
                    x-------x
                    |       |
                    |       0
                    |      /|\\
                    |
                    |
                """,

        """
                    x-------x
                    |       |
                    |       0
                    |       |
                    |
                    |
                """,
        """
                    x-------x
                    |       |
                    |       0
                    |
                    |
                    |
                """,
        """
                    x-------x
                    |
                    |
                    |
                    |
                    |
                """,
        """
                    x-------x
                """
    ]
    return stages[tries - 1]


def play(word):
    """
    a function that execute the game
    :param word: hidden word
    :type: string
    :return: None
    """
    tries = 7
    guessed = False
    guessed_letters = []
    print(display_hangman(tries))
    print(show_hidden_word(word, guessed_letters))
    while not guessed and tries > 0:
        user_guess = input("Guess a letter: ").lower()
        if is_valid_input(user_guess):
            if user_guess not in word:
                tries -= 1
                print(": (")
                print(display_hangman(tries))
                print(show_hidden_word(word, guessed_letters))
                if tries == 1:
                    print("LOSE")
                    exit()
                guessed_letters.append(user_guess)
            elif user_guess in guessed_letters:
                false(guessed_letters, user_guess)
            else:
                guessed_letters.append(user_guess)
                lined_word = show_hidden_word(word, guessed_letters)
                print(lined_word)
                if "_" not in lined_word:
                    guessed = True
        else:
            print("X")
    if guessed is True:
        print("WIN")
    return None


def main():
    print("""Hello, this is a hangman game,
    Please play the game and enjoy!\n""")
    file_path = input("Enter file path: ")
    index = input("Enter index: ")
    print("\nLet’s start!")

    with open(file_path) as file:
        data = file.readlines()
        long_data = (" ".join(data).split(" ")) * 15
    play(long_data[int(index) - 1])


if __name__ == "__main__":
    main()
